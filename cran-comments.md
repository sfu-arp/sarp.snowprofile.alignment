## Resubmission
This is a resubmission of a new version of an existing package. 
It updates the package from version 1.2.2 to 2.0.2.
In this resubmission, I have:

 * fixed missing package anchors
 * removed a unicode character that broke LaTex compilation.

## Changes in version 2.0.0

 * Optimized runtimes through more efficient code and parallel computations
 * Added new reference to CITATION
 * Enhanced functionality as documented in NEWS.md file.

## Test environments

* local Linux Mint 19.1 install (Ubuntu 18.04), R 4.4.1
* `devtools::check_win_devel()`
* `rhub::rc_submit()` 
    - all platforms
    - see https://github.com/r-hub2/quenched-semitheatric-rabidsquirrel-sarp.snowprofile.alignment/actions


## R CMD check result

There were no ERRORs, WARNINGs, or NOTEs on most of the platforms checked. Three platforms failed,
which was not related to our package, but rather due to rhub configuration.

`devtools::spell_check()` has been run and successfully checked.


## Downstream dependencies
There are currently no downstream dependencies for this package
